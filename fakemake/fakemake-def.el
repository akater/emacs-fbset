;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'fbset
  authors "Dima Akater"
  first-publication-year-as-string "2023"
  org-files-in-order '("fbset")
  site-lisp-config-prefix "50"
  license "GPL-3")
